#!/usr/bin/env python

"""A plug-in to handle average number reporting."""

from __future__ import unicode_literals
from __future__ import division
# don't convert to ascii in py2.7 when creating string to return
import redis
import logging

crontable = []
outputs = []

redis_client = redis.Redis(host='localhost', db=0)


def process_message(data):
    '''Calculate average of user.

    When a use enters an integer, we store only the sum and count.
    Then we return the average :-)'''
    # Verify only an integer was typed in
    if data['text'].isdigit():
        # Store sum and count
        redis_client.hincrby('sum', data['user'], int(data['text']))
        redis_client.hincrby('count', data['user'], 1)
        logging.debug('Added {} to user {}'.format(data['text'], data['user']))

        # Get values of user
        pipe = redis_client.pipeline()
        pipe.hget('sum', data['user'])
        pipe.hget('count', data['user'])
        user_sum, user_count = pipe.execute()

        # Calculate the average for the user
        avg_value = int(user_sum) / int(user_count)
        # Return to user
        outputs.append([data['channel'], str(avg_value)])

        # Mark we updated something
        redis_client.setex('movement', 1, 60)
