#!/usr/bin/env python

"""A plug-in to report every minute the total average."""
from __future__ import unicode_literals
from __future__ import division
# don't convert to ascii in py2.7 when creating string to return
import redis

crontable = []
outputs = []

channel = 'C2A8GUR89'

crontable.append([60, "notify_average"])

redis_client = redis.Redis(host='localhost', db=0)


def notify_average():
    '''Calculate total average and publish it.

    Calculate the average for all users and publish if there
    has been any udpate.'''

    # Check if any updates happend in the last minute
    if redis_client.get('movement'):
        # Get all sum and count values
        total_sum = sum([int(v) for v in redis_client.hgetall('sum').values()])
        total_count = sum([int(v) for v in
                           redis_client.hgetall('count').values()])
        # Calculate average
        total_avg = total_sum / total_count
        # Publish to channel
        outputs.append([channel, str(total_avg)])
